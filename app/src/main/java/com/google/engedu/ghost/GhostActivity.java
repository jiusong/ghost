/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.engedu.ghost;

import android.content.res.AssetManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.google.engedu.ghost.databinding.ActivityGhostBinding;
import java.io.InputStream;
import java.util.Random;


public class GhostActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = GhostActivity.class.getName();
    private ActivityGhostBinding binding;
    private static final String COMPUTER_TURN = "Computer's turn";
    private static final String USER_TURN = "Your turn";
    private GhostDictionary dictionary;
    private boolean userTurn = false;
    private Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ghost);
        initUI();
        AssetManager assetManager = getAssets();
        /**
         **
         **  YOUR CODE GOES HERE
         **  Load word list from assets.
         **/
        try {
            InputStream inputStream = assetManager.open("words.txt");
            dictionary = new FastDictionary(inputStream);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        onStart(null);
    }

    private void initUI() {
        binding.buttonChallenge.setOnClickListener(this);
        binding.buttonRestart.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ghost, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Handler for the "Reset" button.
     * Randomly determines whether the game starts with a user turn or a computer turn.
     * @param view
     * @return true
     */
    public boolean onStart(View view) {
        userTurn = random.nextBoolean();
        TextView text = (TextView) findViewById(R.id.ghostText);
        text.setText("");
        TextView label = (TextView) findViewById(R.id.gameStatus);
        if (userTurn) {
            label.setText(USER_TURN);
        } else {
            label.setText(COMPUTER_TURN);
            computerTurn();
        }
        return true;
    }

    private void computerTurn() {
        TextView label = (TextView) findViewById(R.id.gameStatus);
        // Check if the fragment is a word
        String fragment = binding.ghostText.getText().toString();
        if (dictionary.isWord(fragment)) {
            binding.gameStatus.setText("Computer Won!");
            return;
        }
        // get possible words with prefix
        String anyWordStartingWith = dictionary.getAnyWordStartingWith(fragment);
        if (anyWordStartingWith == null) {
            binding.gameStatus.setText("Computer Won!");
            return;
        } else {
            String newFragment = fragment + anyWordStartingWith.charAt(fragment.length());
            binding.ghostText.setText(newFragment);
        }
        userTurn = true;
        label.setText(USER_TURN);
    }

    /**
     * Handler for user key presses.
     * @param keyCode
     * @param event
     * @return whether the key stroke was handled.
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        /**
         **
         **  YOUR CODE GOES HERE
         **
         **/
        char pressedKey = (char) event.getUnicodeChar();
        if (pressedKey >= 'a' && pressedKey <= 'z') {
            String word = binding.ghostText.getText().toString();
            String newWord = word + pressedKey;
            binding.ghostText.setText(newWord);
            computerTurn();
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == binding.buttonRestart.getId()) {
            onStart(null);
        } else if (v.getId() == binding.buttonChallenge.getId()) {
            challengeComputer();
        }
    }

    private void challengeComputer() {
        String displayWord = binding.ghostText.getText().toString();
        if (dictionary.isWord(displayWord)
                || dictionary.getAnyWordStartingWith(displayWord) == null) {
            binding.gameStatus.setText("You Won!");
        } else {
            binding.ghostText.setText(dictionary.getAnyWordStartingWith(displayWord));
            binding.gameStatus.setText("Computer Won");
        }
    }
}
