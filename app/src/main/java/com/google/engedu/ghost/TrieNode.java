/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.engedu.ghost;

import java.util.HashMap;
import java.util.Set;


public class TrieNode {
    private HashMap<Character, TrieNode> children;
    private boolean isWord;

    public TrieNode() {
        children = new HashMap<>();
        isWord = false;
    }

    public void add(String s) {
        // Edge cases.
        if (s == null || s.length() == 0) {
            return;
        }
        TrieNode child = children.get(s.charAt(0));
        if (child == null) {
            // Add a new child.
            TrieNode node = new TrieNode();
            children.put(s.charAt(0), node);
            if (s.length() == 1) {
                node.isWord = true;
            } else {
                node.add(s.substring(1, s.length()));
            }
        } else {
            // Add to existing child.
            child.add(s.substring(1, s.length()));
        }
    }

    public boolean isWord(String s) {
        if (s.length() > 1) {
            TrieNode node = children.get(s.charAt(0));
            if (node != null) {
                return node.isWord(s.substring(1, s.length()));
            } else {
                return false;
            }
        }
        if (s.length() == 1) {
            TrieNode node = children.get(s.charAt(0));
            if (node != null && node.isWord) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public String getAnyWordStartingWith(String s) {

        int len = s.length();
        StringBuffer word = new StringBuffer();
        TrieNode node = this;
        for (int i = 0; i < len; i++) {
            word.append(s.charAt(i));
            node = node.children.get(s.charAt(i));
            if(node != null) {
                continue;
            } else {
                return null;
            }
        }

        if (node.isWord) {
            return s;
        }

        while (node != null) {
            if (node.children.size() > 0) {
                Character key = node.children.keySet().iterator().next();
                word.append(key);
                node = node.children.get(key);
            } else {
                break;
            }
        }

        if (word.length() == s.length()) {
            return null;
        }

        return word.toString();
    }

    public String getGoodWordStartingWith(String s) {
        int len = s.length();
        StringBuffer word = new StringBuffer();
        TrieNode node = this;
        for (int i = 0; i < len; i++) {
            word.append(s.charAt(i));
            node = node.children.get(s.substring(i, i +1));
            if(node != null) {
                continue;
            } else {
                return null;
            }
        }

        Set<Character> keySets = node.children.keySet();
        TrieNode goodWordNode = null;
        for (Character key : keySets) {
            if (!node.children.get(key).isWord) {
                goodWordNode = node.children.get(key);
                break;
            }
        }

        if (goodWordNode == null) {
            goodWordNode = node;
        }

        while (goodWordNode != null) {
            if (goodWordNode.children.size() > 0) {
                Character key = goodWordNode.children.keySet().iterator().next();
                word.append(key);
                goodWordNode = goodWordNode.children.get(key);
            } else {
                break;
            }
        }

        if (word.length() == s.length()
                && !node.isWord) {
            return null;
        }

        return word.toString();
    }
}
