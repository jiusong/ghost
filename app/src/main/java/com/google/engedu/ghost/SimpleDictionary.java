/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.engedu.ghost;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SimpleDictionary implements GhostDictionary {
    private ArrayList<String> words;

    public SimpleDictionary(InputStream wordListStream) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(wordListStream));
        words = new ArrayList<>();
        String line = null;
        while((line = in.readLine()) != null) {
            String word = line.trim();
            if (word.length() >= MIN_WORD_LENGTH)
              words.add(line.trim());
        }
    }

    @Override
    public boolean isWord(String word) {
        return words.contains(word);
    }

    @Override
    public String getAnyWordStartingWith(String prefix) {
        String word;
        if (prefix == null || prefix.length() == 0) {
            Random random = new Random();
            word = words.get(random.nextInt(words.size()));
        } else {
            word = binarySearchPrefixWord(prefix);
        }
        return word;
    }

    private String binarySearchPrefixWord(String prefix) {
        int start = 0;
        int end = words.size() -1;
        while (start <= end) {
            int mid = start + (end - start)/2;
            String word = words.get(mid);
            String subStr = word.substring(0, prefix.length());
            if (subStr.equals(prefix)) {
                return word;
            }
            if (compare(subStr, prefix)) {
                end = mid -1;
            } else {
                start = mid +1;
            }
        }
        return null;
    }

    private boolean compare(String subString, String prefix) {
        if (subString.length() < prefix.length()) {
            return false;
        }
        for (int i = 0; i < prefix.length(); i++) {
            if (subString.charAt(i) > prefix.charAt(i)) {
                return true;
            }
            if (subString.charAt(i) < prefix.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String getGoodWordStartingWith(String prefix) {
        String selected = null;
        // Get whole range of words that start with the prefix.
        List<String> words = getAllWordsStartingWith(prefix);
        // Divide the words between odd lengths and even lengths.
        List<String> oddWords = new ArrayList<>();
        List<String> evenWords = new ArrayList<>();
        for (String word : words) {
            if (word.length() % 2 == 0) {
                evenWords.add(word);
            } else {
                oddWords.add(word);
            }
        }
        // Randomly selecting a word from the appropriate set.
        selected = getRandomWordStartWith(oddWords, evenWords);
        return selected;
    }

    private List<String> getAllWordsStartingWith(String prefix) {
        List<String> results = new ArrayList<>();
        int start = 0;
        int end = words.size();
        int startPos = 0;
        int endPos = 0;
        // Find the position of the first word starts with the given prefix.
        while (start <= end) {
            int mid = start + (end - start)/2;
            String word = words.get(mid);
            // Reached the start of the list.
            if (mid == 0) {
                startPos = mid;
                break;
            }
            String preWord = words.get(mid -1);
            if (word.startsWith(prefix)) {
                if (!preWord.startsWith(prefix)) {
                    startPos = mid -1;
                    break;
                }
                if (preWord.startsWith(prefix)) {
                    end = mid -1;
                    continue;
                }
            }
            if (compare(word, prefix)) {
                end = mid -1;
            } else {
                start = mid +1;
            }
        }
        // Find the position of the last word starts with the given prefix.
        start = 0;
        end = words.size();
        while (start <= end) {
            int mid = start + (end - start)/2;
            String word = words.get(mid);
            // Reached the end of the list.
            if (mid == words.size() -1) {
                endPos = mid;
                break;
            }
            String nextWord = words.get(mid +1);
            if (word.startsWith(prefix)) {
                if (!nextWord.startsWith(prefix)) {
                    endPos = mid +1;
                    break;
                }
                if (nextWord.startsWith(prefix)) {
                    start = mid +1;
                    continue;
                }
            }
            if (compare(word, prefix)) {
                end = mid -1;
            } else {
                start = mid +1;
            }
        }
        // Get all the words starts with the given prefix.
        for (int i = startPos; i < endPos; i++) {
            results.add(words.get(i));
        }
        return results;
    }

    private String getRandomWordStartWith(List<String> oddWords, List<String> evenWords) {
        String word = null;
        int oddSize = oddWords.size();
        int evenSize = evenWords.size();
        Random random = new Random();
        if (random.nextInt(2) == 0) {
            if (oddSize == 0) {
                return word;
            }
            word = oddWords.get(random.nextInt(oddSize));
        } else {
            if (evenSize == 0) {
                return word;
            }
            word = evenWords.get(random.nextInt(evenSize));
        }
        return word;
    }

    private int binarySearch(int value, List<Integer> list) {
        int low = 0;
        int high = list.size() -1;
        while (low <= high) {
            int mid = (low + high)/2;
            if (value == list.get(mid)) {
                return mid;
            }
            if (value < list.get(mid)) {
                high = mid -1;
            }
            if (value > list.get(mid)) {
                low = mid +1;
            }
         }
         return -1;
    }
}
